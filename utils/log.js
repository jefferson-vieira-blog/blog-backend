/* eslint-disable */

require('colors');

module.exports = {
  success: msg => console.info(msg.green),
  warning: msg => console.warn(msg.yellow),
  error: msg => console.error(msg.red),
  normal: msg => console.info(msg)
};
