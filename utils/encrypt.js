const bcrypt = require('bcrypt-nodejs');

module.exports = password => {
  const salt = bcrypt.genSaltSync();
  return bcrypt.hashSync(password, salt);
};
