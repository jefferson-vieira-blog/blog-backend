module.exports = async (getQuery, countQuery, size = 10, page = 0) => {
  try {
    const content = await getQuery.limit(size).offset(page * size);
    const { count } = await countQuery;

    const itens = content.length;
    const totalItens = parseInt(count);
    const totalPages = Math.ceil(totalItens / size);
    const first = page === 0;
    const last = page === totalPages - 1;

    return {
      content,
      itens,
      totalItens,
      size,
      page,
      totalPages,
      first,
      last
    };
  } catch (error) {
    throw error;
  }
};
