const mongoose = require('mongoose');

const log = require('../utils/log');
const { mongo } = require('../.env');

mongoose
  .connect(
    mongo,
    { useNewUrlParser: true }
  )
  .then(() => {
    const msg = '[mongo] connection success';
    log.success(msg);
  })
  .catch(error => {
    const msg = '[mongo] connection error';
    log.error(msg);
    log.normal(error.toString());
  });
