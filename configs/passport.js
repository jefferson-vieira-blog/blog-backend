const passport = require('passport');
const { Strategy, ExtractJwt } = require('passport-jwt');

const { authSecret } = require('../.env');

module.exports = app => {
  const params = {
    secretOrKey: authSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
  };

  const strategy = new Strategy(params, async (payload, done) => {
    try {
      const user = await app
        .db('users')
        .where({ id: payload.id })
        .first();
      return done(null, user ? { ...payload } : false);
    } catch (error) {
      return done(error, false);
    }
  });

  passport.use(strategy);

  return {
    authenticate: () => passport.authenticate('jwt', { session: false })
  };
};
