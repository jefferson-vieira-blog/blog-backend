module.exports = () => (req, res, next) => {
  const { user } = req;

  if (user && user.admin) return next();
  return res.status(401).send('O usuário não é um administrador!');
};
