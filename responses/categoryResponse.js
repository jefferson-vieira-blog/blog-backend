const getParent = (categories, parentId) => {
  const parent = categories.filter(category => category.id === parentId);
  return parent.pop() || null;
};

const withPath = categories =>
  categories.map(category => {
    let path = category;
    let parent = getParent(categories, category.parent_id);

    while (parent) {
      path.parent = parent;
      path = parent;
      parent = getParent(categories, parent.parent_id);
    }

    path.parent = null;
    delete category.parent_id;
    return category;
  });

module.exports = () => async (req, res) => {
  const { categories } = res.locals;

  categories.content = withPath(categories.content);

  return res.json(categories);
};
