module.exports = app => {
  const save = user =>
    app
      .db('users')
      .returning('id')
      .insert(user);

  const getAll = () =>
    app
      .db('users')
      .select('id', 'name', 'email', 'admin')
      .whereNull('deleted_at');

  const getBy = user =>
    app
      .db('users')
      .select('id', 'name', 'email', 'admin')
      .whereNull('deleted_at')
      .where(user)
      .first();

  const update = (id, user) =>
    app
      .db('users')
      .update(user)
      .where({ id });

  const remove = id =>
    app
      .db('users')
      .update({ deleted_at: new Date() })
      .where({ id });

  const count = () =>
    app
      .db('users')
      .whereNull('deleted_at')
      .count('id')
      .first();

  return { save, getAll, getBy, update, remove, count };
};
