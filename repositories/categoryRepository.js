module.exports = app => {
  const { categoryQuery } = app.queries;

  const save = user =>
    app
      .db('categories')
      .returning('id')
      .insert(user);

  const getAll = () => app.db('categories');

  const getBy = user =>
    app
      .db('categories')
      .where(user)
      .first();

  const getTreeById = id =>
    app.db.raw(categoryQuery.CATEGORY_WITH_CHILDREN, id);

  const update = (id, user) =>
    app
      .db('categories')
      .update(user)
      .where({ id });

  const remove = id =>
    app
      .db('categories')
      .where({ id })
      .del();

  const count = () =>
    app
      .db('categories')
      .count('id')
      .first();

  return { save, getAll, getBy, getTreeById, update, remove, count };
};
