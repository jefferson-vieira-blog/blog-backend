module.exports = app => {
  const save = article =>
    app
      .db('articles')
      .returning('id')
      .insert(article);

  const getAll = () => app.db('articles');

  const getBy = article =>
    app
      .db('articles')
      .where(article)
      .first();

  const getByCategoriesIds = categoriesIds =>
    app
      .db({ a: 'articles', u: 'users' })
      .select(
        'a.id',
        'a.name',
        'a.description',
        'a.category_id',
        'a.image_url',
        {
          author: 'u.name'
        }
      )
      .whereRaw('?? = ??', ['u.id', 'a.user_id'])
      .whereIn('a.category_id', categoriesIds)
      .orderBy('a.id', 'desc');

  const update = (id, article) =>
    app
      .db('articles')
      .update(article)
      .where({ id });

  const remove = id =>
    app
      .db('articles')
      .update({ deleted_at: new Date() })
      .where({ id });

  const count = () =>
    app
      .db('articles')
      .count('id')
      .first();

  return { save, getAll, getBy, getByCategoriesIds, update, remove, count };
};
