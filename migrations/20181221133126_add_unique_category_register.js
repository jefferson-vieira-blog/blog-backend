exports.up = knex =>
  knex.schema.alterTable('categories', table => {
    table.unique(['name', 'parent_id']);
  });

exports.down = knex =>
  knex.schema.alterTable('categories', table => {
    table.dropUnique(['name', 'parent_id']);
  });
