exports.up = knex =>
  knex.schema.createTable('articles', table => {
    table.increments('id').primary();
    table.string('name').notNull();
    table.string('description', 1024).notNull();
    table.string('image_url', 1024);
    table.binary('content', 1024).notNull();
    table
      .integer('user_id')
      .references('id')
      .inTable('users')
      .notNull();
    table
      .integer('category_id')
      .references('id')
      .inTable('categories')
      .notNull();
  });

exports.down = knex => knex.schema.dropTable('articles');
