module.exports = app => (value, msg) => {
  const { existsOrError } = app.validators;
  try {
    existsOrError(value, msg);
  } catch (error) {
    return;
  }
  throw msg;
};
