module.exports = () => (value, msg) => {
  if (!Number.isInteger(value)) throw msg;
};
