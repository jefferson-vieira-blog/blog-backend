module.exports = () => (value, msg) => {
  if (!value) throw msg;
  if (typeof value === 'string' && !value.trim()) throw msg;
  if (Array.isArray(value) && value.length === 0) throw msg;
};
