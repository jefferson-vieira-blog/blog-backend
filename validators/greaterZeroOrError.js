module.exports = () => value => {
  const msg = 'Precisa ser um número maior que zero!';
  const val = +value;

  if (!val) throw msg;
  if (Number.isNaN(val)) throw msg;
  if (val < 1) throw msg;
};
