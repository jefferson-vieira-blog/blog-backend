module.exports = () => (...values) => {
  const first = values.shift();
  const msg = values.pop();

  if (!values.every(value => value === first)) throw msg;
};
