const schedule = require('node-schedule');

const log = require('../utils/log');

module.exports = app => {
  schedule.scheduleJob('*/1 * * * *', async () => {
    try {
      log.warning('[stats] update running...');

      const { count: usersCount } = await app
        .db('users')
        .count('id')
        .first();
      const { count: categoriesCount } = await app
        .db('categories')
        .count('id')
        .first();
      const { count: articlesCount } = await app
        .db('articles')
        .count('id')
        .first();

      const { statModel: Stat } = app.models;
      const lastStat = await Stat.findOne({}, {}, { sort: { createdAt: -1 } });
      const stat = new Stat({
        users: usersCount,
        categories: categoriesCount,
        articles: articlesCount,
        createdAt: new Date()
      });

      if (
        !lastStat ||
        lastStat.users !== stat.users ||
        lastStat.categories !== stat.categories ||
        lastStat.articles !== stat.articles
      ) {
        await stat.save();
        log.success('[stats] update success');
      } else {
        log.normal('[stats] already up to date');
      }
    } catch (error) {
      log.error('[stats] update error');
      log.normal(error.toString());
    }
  });
};
