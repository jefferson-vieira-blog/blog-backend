const express = require('express');
const consign = require('consign');
const mongoose = require('mongoose');
const db = require('./configs/db');
const log = require('./utils/log');

const app = express();

app.mongoose = mongoose;
app.db = db;

consign()
  .then('./configs')
  .then('./queries')
  .then('./validators')
  .then('./models')
  .then('./schedule')
  .then('./requests')
  .then('./repositories')
  .then('./services')
  .then('./facades')
  .then('./responses')
  .then('./controllers')
  .into(app);

const port = process.env.PORT || '3000';

app.listen(port, () => {
  log.success(`[app] listen on port ${port}...`);
});
