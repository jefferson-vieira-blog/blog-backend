module.exports = app => async (req, res, next) => {
  const { userModel: User } = app.models;
  const { existsOrError, isMailOrError, equalsOrError } = app.validators;

  const { method, body: userRequest } = req;

  try {
    const { name, email, password, confirmPassword } = userRequest;
    existsOrError(name, 'Nome não informado!');
    existsOrError(email, 'E-mail não informado!');
    isMailOrError(email, 'E-mail inválido!');
    if (method === 'POST') {
      existsOrError(password, 'Senha não informada!');
      existsOrError(confirmPassword, 'Confirmar a senha é necessário!');
      equalsOrError(password, confirmPassword, 'As senhas não conferem!');
    }
  } catch (error) {
    return res.status(400).send(error);
  }

  const user = new User(userRequest);
  if (!req.originalUrl.startsWith('/users')) user.admin = false;

  res.locals.user = user;
  return next();
};
