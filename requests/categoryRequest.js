module.exports = app => async (req, res, next) => {
  const { categoryModel: Category } = app.models;
  const { existsOrError, integerOrError } = app.validators;

  const { body: categoryRequest } = req;

  try {
    const { name, parentId } = categoryRequest;
    existsOrError(name, 'Nome da categoria não informado!');
    if (parentId) integerOrError(parentId, 'Categoria pai inválida!');
  } catch (error) {
    return res.status(400).send(error);
  }

  const category = new Category(categoryRequest);

  res.locals.category = category;
  return next();
};
