module.exports = app => {
  const { statModel: Stat } = app.models;

  const get = async (req, res) => {
    try {
      const stat = await Stat.findOne({}, {}, { sort: { createdAt: -1 } });
      const defaukt = { users: 0, categories: 0, articles: 0 };
      return res.json(stat || defaukt);
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  return { get };
};
