const encryptPassword = require('../utils/encrypt');
const paginate = require('../utils/paginate');

module.exports = app => {
  const { userRepository: repository } = app.repositories;

  const { existsOrError, notExistsOrError } = app.validators;

  const save = async (req, res) => {
    const { user } = res.locals;

    try {
      const userRetrived = await repository.getBy({ email: user.email });
      notExistsOrError(userRetrived, 'Usuário já cadastrado!');
    } catch (error) {
      return res.status(409).send(error);
    }

    user.password = encryptPassword(user.password);

    try {
      const id = await repository.save(user);
      return res
        .status(201)
        .append('Location', `/users/${id}`)
        .send();
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const getAll = async (req, res) => {
    const size = parseInt(req.query.size) || 10;
    const page = parseInt(req.query.page) || 0;

    try {
      const users = await paginate(
        repository.getAll(),
        repository.count(),
        size,
        page
      );
      return res.json(users);
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const getById = async (req, res) => {
    const { id } = req.params;

    try {
      const user = await repository.getBy({ id });
      return res.json(user);
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const update = async (req, res) => {
    const { id } = req.params;
    const { user } = res.locals;

    try {
      const userRetrived = await repository.getBy({ id });
      existsOrError(userRetrived, 'Usuário não encontrado!');
    } catch (error) {
      return res.status(400).send(error);
    }

    user.password = encryptPassword(user.password);

    try {
      await repository.update(user);
      return res.status(204).send();
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const remove = async (req, res) => {
    const { id } = req.params;

    try {
      const rowsDeleted = await repository.remove(id);
      return rowsDeleted;
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  return { save, getAll, getById, update, remove };
};
