const jwt = require('jwt-simple');
const bcrypt = require('bcrypt-nodejs');

const { authSecret } = require('../.env');

module.exports = app => {
  const { existsOrError } = app.validators;

  const signin = async (req, res) => {
    const { email, password } = req.body;

    try {
      existsOrError(email);
      existsOrError(password);
    } catch (error) {
      return res.status(400).send('Informe o usuário e a senha!');
    }

    const user = await app
      .db('users')
      .where({ email })
      .first();

    if (!user) return res.status(400).send('Usuário não encontrado!');

    const isMatch = bcrypt.compareSync(password, user.password);
    if (!isMatch) return res.status(401).send('E-mail ou senha inválido(s)!');

    const now = Date.now() / 1000;

    const payload = {
      id: user.id,
      name: user.name,
      email: user.email,
      admin: user.admin,
      iat: now,
      exp: now + 60 * 60 * 24 * 1
    };

    return res.json({
      ...payload,
      token: jwt.encode(payload, authSecret)
    });
  };

  const validateToken = async (req, res) => {
    try {
      const userData = req.body || null;
      existsOrError(userData, 'Nao foi possível recuperar o usuário!');

      const token = jwt.decode(userData.token, authSecret);
      if (!new Date(token.exp * 1000) > new Date())
        throw new Error('Token expirado!');

      return res.json(token);
    } catch (error) {
      return res.send(false);
    }
  };

  return { signin, validateToken };
};
