const paginate = require('../utils/paginate');

const categoryService = require('./categoryService');

module.exports = app => {
  const { articleRepository: repository } = app.repositories;

  const { existsOrError } = app.validators;

  const save = async (req, res) => {
    const {
      params: { id: newId },
      body: article
    } = req;

    if (newId) article.id = newId;
    try {
      existsOrError(article.name, 'Nome não informado!');
      existsOrError(article.description, 'Descrição não informada!');
      existsOrError(article.category_id, 'Categoria não informada!');
      existsOrError(article.content, 'Conteúdo não informado!');
    } catch (error) {
      res.status(400).send(error);
    }

    try {
      if (article.id) {
        await app
          .db('articles')
          .update(article)
          .where({ id: article.id });
      } else {
        await app.db('articles').insert(article);
      }
      return res.status(204).send();
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const getAll = async (req, res) => {
    const size = parseInt(req.query.size) || 10;
    const page = parseInt(req.query.page) || 0;

    try {
      const articles = await paginate(
        repository.getAll(),
        repository.count(),
        size,
        page
      );
      return res.json(articles);
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const getById = async (req, res) => {
    const {
      params: { id }
    } = req;

    try {
      const article = await app
        .db('articles')
        .where({ id })
        .first();

      // response
      article.content = article.content.toString();
      return res.json(article);
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const getByCategoryId = async (req, res) => {
    const { id } = req.params;
    const size = parseInt(req.query.size) || 10;
    const page = parseInt(req.query.page) || 0;

    try {
      const categoriesIds = await categoryService(app).getTreeById(id);
      const articles = await paginate(
        repository.getByCategoriesIds(categoriesIds),
        repository.count(),
        size,
        page
      );

      return articles;
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const getByUserId = async (req, res) => {
    const { id } = req.params;

    try {
      const articles = await app.db('articles').where({ user_id: id });
      return articles;
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const remove = async (req, res) => {
    try {
      const { id } = req.params;

      const rowsDeleted = await app
        .db('articles')
        .where({ id })
        .del();
      existsOrError(rowsDeleted, 'Artigo não encontrado!');

      return res.status(204).send();
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  return { save, getAll, getById, getByCategoryId, getByUserId, remove };
};
