const paginate = require('../utils/paginate');

const toTree = (categories, tree) => {
  let newTree = tree;

  if (!newTree) newTree = categories.filter(category => !category.parent_id);

  newTree = newTree.map(parentCategory => {
    const isChild = category => category.parent_id === parentCategory.id;
    const children = toTree(categories, categories.filter(isChild));
    return { ...parentCategory, children };
  });

  return newTree;
};

module.exports = app => {
  const { categoryRepository: repository } = app.repositories;

  const { existsOrError, notExistsOrError } = app.validators;

  const save = async (req, res) => {
    const { category } = res.locals;

    try {
      const categoryRetrived = await repository.getBy(category, res);
      notExistsOrError(categoryRetrived, 'Categoria já cadastrada!');
    } catch (error) {
      return res.status(409).send(error);
    }

    try {
      const id = await repository.save(category);
      return res
        .status(201)
        .append('Location', `/categories/${id}`)
        .send();
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const getAll = async (req, res) => {
    const size = parseInt(req.query.size) || 10;
    const page = parseInt(req.query.page) || 0;

    try {
      const categories = await paginate(
        repository.getAll(),
        repository.count(),
        size,
        page
      );
      return categories;
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const getById = async (req, res) => {
    const { id } = req.params;

    try {
      const category = await repository.getBy({ id });
      return res.json(category);
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const getTree = async (req, res) => {
    try {
      const categories = await repository.getAll();
      return res.json(toTree(categories));
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const getTreeById = async id => {
    try {
      const categories = await repository.getTreeById(id);
      const categoriesIds = categories.rows.map(c => c.id);
      return categoriesIds;
    } catch (error) {
      throw error;
    }
  };

  const update = async (req, res) => {
    const { id } = req.params;
    const { category } = res.locals;

    try {
      const categoryRetrived = await repository.getBy({ id });
      existsOrError(categoryRetrived, 'Categoria não encontrada!');
    } catch (error) {
      return res.status(400).send(error);
    }

    try {
      await repository.update(category);
      return res.status(204).send();
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const remove = async (req, res) => {
    const { id } = req.params;

    try {
      const categories = await repository.getBy({ parent_id: id });
      notExistsOrError(categories, 'Essa categoria possui subcategorias!');
    } catch (error) {
      return res.status(409).send(error);
    }

    try {
      const rowsDeleted = await repository.remove(id);
      return rowsDeleted;
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  return { save, getAll, getById, getTree, getTreeById, update, remove };
};
