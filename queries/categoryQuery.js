module.exports = () => ({
  CATEGORY_WITH_CHILDREN: `
    WITH RECURSIVE subcategories (id) AS (
      SELECT id
      FROM categories
      WHERE id = ?
      UNION ALL
      SELECT c.id
      FROM subcategories s, categories c
      WHERE c.parent_id = s.id
    )
    SELECT id
    FROM subcategories
  `
});
