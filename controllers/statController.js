module.exports = app => {
  const { authenticate } = app.configs.passport;
  const { get } = app.services.statService;

  app
    .route('/stats')
    .all(authenticate())
    .get(get);
};
