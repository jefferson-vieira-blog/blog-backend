module.exports = app => {
  const { signin, signup, validateToken } = app.facades.authFacade;

  const { userRequest: request } = app.requests;

  app.route('/signin').post(signin);

  app.route('/signup').post(request, signup);

  app.route('/validate-token').post(validateToken);
};
