module.exports = app => {
  const {
    admin,
    passport: { authenticate }
  } = app.configs;
  const { userRequest: request } = app.requests;
  const { save, getAll, getById, update, remove } = app.facades.userFacade;

  app
    .route('/users')
    .all(authenticate())
    .get(getAll)
    .post(admin, request, save);

  app
    .route('/users/:id')
    .all(authenticate())
    .get(admin, getById)
    .put(admin, request, update)
    .delete(remove);
};
