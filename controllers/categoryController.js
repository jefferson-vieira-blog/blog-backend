module.exports = app => {
  const {
    admin,
    passport: { authenticate }
  } = app.configs;
  const { categoryRequest: request } = app.requests;
  const {
    save,
    getAll,
    getById,
    getTree,
    getArticles,
    update,
    remove
  } = app.facades.categoryFacade;
  const { categoryResponse: response } = app.responses;

  app
    .route('/categories')
    .all(authenticate())
    .get(getAll, response)
    .post(admin, request, save);

  app
    .route('/categories/tree')
    .all(authenticate())
    .get(getTree);

  app
    .route('/categories/:id')
    .all(authenticate())
    .get(getById)
    .put(admin, request, update)
    .delete(remove);

  app
    .route('/categories/:id/articles')
    .all(authenticate())
    .get(getArticles);
};
