module.exports = app => {
  const {
    admin,
    passport: { authenticate }
  } = app.configs;

  const { save, getAll, getById, remove } = app.services.articleService;

  app
    .route('/articles')
    .all(authenticate())
    .post(admin, save)
    .get(getAll);

  app
    .route('/articles/:id')
    .all(authenticate())
    .get(getById)
    .put(admin, save)
    .delete(admin, remove);
};
