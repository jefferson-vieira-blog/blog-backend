module.exports = app => {
  const {
    categoryService,
    articleService: { getByCategoryId }
  } = app.services;

  const { existsOrError, notExistsOrError } = app.validators;

  const getAll = async (req, res, next) => {
    const categories = await categoryService.getAll(req, res);
    res.locals.categories = categories;
    return next();
  };

  const getArticles = async (req, res) => {
    try {
      const articles = await getByCategoryId(req, res);
      return res.json(articles);
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  const remove = async (req, res) => {
    try {
      const articles = await getByCategoryId(req, res);
      notExistsOrError(articles.content, 'Essa categoria possui artigos!');
    } catch (error) {
      return res.status(409).send(error);
    }

    try {
      const rowsDeleted = await categoryService.remove(req, res);
      existsOrError(rowsDeleted, 'Não foi possível recuperar a categoria!');
      return res.status(204).send();
    } catch (error) {
      return res.status(400).send(error);
    }
  };

  return {
    ...categoryService,
    getAll,
    getArticles,
    remove
  };
};
