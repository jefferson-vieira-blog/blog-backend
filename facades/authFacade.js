module.exports = app => {
  const {
    authService,
    userService: { save: signup }
  } = app.services;

  return {
    ...authService,
    signup
  };
};
