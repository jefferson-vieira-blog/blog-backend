module.exports = app => {
  const {
    userService,
    articleService: { getByUserId }
  } = app.services;
  const { existsOrError, notExistsOrError } = app.validators;

  const remove = async (req, res) => {
    try {
      const articles = await getByUserId(req, res);
      notExistsOrError(articles, 'Usuário possui artigos cadastrados!');
    } catch (error) {
      return res.status(409).send(error);
    }

    try {
      const rowsUpdated = await userService.remove(req, res);
      existsOrError(rowsUpdated, 'Não foi possível recuperar o usuário!');
      return res.status(204).send();
    } catch (error) {
      return res.status(400).send(error);
    }
  };

  return {
    ...userService,
    remove
  };
};
