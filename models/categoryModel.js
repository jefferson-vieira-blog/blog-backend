module.exports = () =>
  class Category {
    constructor({ name, parentId }) {
      this.name = name;
      if (parentId) this.parent_id = parentId;
    }
  };
