module.exports = () =>
  class User {
    constructor({ name, email, password, admin }) {
      this.name = name;
      this.email = email;
      if (password) this.password = password;
      this.admin = admin;
    }
  };
